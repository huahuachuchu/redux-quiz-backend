package com.twuc.webApp.contracts;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreatePostRequest {
    @NotNull
    @Size(min = 1, max = 128)
    private String title;

    @NotNull
    @Size(min = 1, max = 65536)
    private String description;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
